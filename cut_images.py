from PIL import Image
import openslide
import cv2
import numpy as np
import imutils
import csv
import time
import logging

# from pprint import pprint

def convert_image(img):
    pil_image = img.convert('RGB')
    open_cv_image = np.array(pil_image)
    # Convert RGB to BGR
    open_cv_image = open_cv_image[:, :, ::-1].copy()
    return open_cv_image

def writeImage(img,name):
    open_cv_image = convert_image(img)
    cv2.imwrite(name, open_cv_image )

def imgShow(img,name):
    open_cv_image = convert_image(img)
    cv2.imshow(name,open_cv_image)
        

# def inverte(imagem, name):
#     imagem = abs(imagem - 255)
#     cv2.imwrite(name, imagem)
def inverte(imagem, name):
    imagem = (255-imagem)
    cv2.imwrite(name, imagem)

def inverte2(imagem, name):
    imagem = cv2.bitwise_not(imagem)
    # for x in np.nditer(imagem, op_flags=['readwrite']):
    #     x = abs(x - 255)
    cv2.imwrite(name, imagem)

def rescale_img(img,percent):
    scale_percent = 30 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    return cv2.resize(img, dim, interpolation = cv2.INTER_AREA), dim

def filter_img(_img,min_=160,max_=255):
    img = cv2.cvtColor(_img, cv2.COLOR_BGR2GRAY)
    gray = cv2.bilateralFilter(img, 11, 17, 17)
    # blur = cv2.GaussianBlur(gray,(5,5),0)
    ret, thresh = cv2.threshold(gray, min_,max_ , cv2.THRESH_BINARY)
    return thresh

def get_percent_of_area(hull, img):
    
    _,_,w,h = cv2.boundingRect(hull)
    w_img,h_img = img.shape
    img_area = w_img * h_img
    hull_area= w * h 
    return int(round((hull_area*100.0/img_area)))
    
def verify_same(desired_areas,draw_img = False):

    arr = []
    for desired_area in desired_areas:
        _,_,w,h = cv2.boundingRect(desired_area)
        area = w*h
        arr.append([area,desired_area])
        # print(cv2.boxPoints(cv2.minAreaRect(desired_area)))


    #sort arr
    arr2=[]
    j=0
    while (arr != []):
        i=0
        low_index=0
        lowest_val = arr[low_index][0]
        for element in arr:
            # if(arr2 == []):
            #     lowest_val = element2[0]
            if(lowest_val>element[0]):
                lowest_val = element[0]
                low_index = i
            i+=1
        arr2.append(arr[low_index])
        arr.pop(low_index)        
        j+=1

    # verify if convex hull is inside other
    index_element = 0
    for el1 in arr2:
        rect = cv2.minAreaRect(el1[1])
        # rect_areas.append(rect)
        points = cv2.boxPoints(rect)

        for el2 in arr2:
            count_points=0
            for point in points:
                dist = cv2.pointPolygonTest(el2[1], (point[0], point[1]), True)
                if(dist > 0):
                    count_points+=1
                # elif(dist > -20):
                #     # points.pop(index_point)
                #     count_points_proxmited+=1
            if(count_points>=3):
                arr2.pop(index_element)
    index_element+=1

    img_arr = []
    return_arr = []
    for el in arr2:
        epsilon = 0.1*cv2.arcLength(el[1],True)
        approx = cv2.approxPolyDP(el[1],epsilon,True)
        if(draw_img):
            img_arr.append(approx)
        return_arr.append(cv2.boundingRect(approx))
    
    return return_arr,img_arr
        

def get_desired_areas(_img,img_draw=False):
    #get contours
    contours, hierarchy = cv2.findContours(_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    hull = []
    
    # calculate points for each contour
    for i in range(len(contours)):
        # creating convex hull object for each contour
        hull.append(cv2.convexHull(contours[i], False))

    if (img_draw):
        # create an empty black image
        drawing = np.zeros((_img.shape[0], _img.shape[1], 3), np.uint8)

    desired_areas = []
    # draw contours and hull points
    for i in range(len(contours)):
        if (img_draw):
            color_contours = (0, 255, 0) # green - color for contours
            color = (255, 0, 0) # blue - color for convex hull
            # draw ith contourverify_same(desired_areas)
            cv2.drawContours(drawing, contours, i, color_contours, 1, 8, hierarchy)
        
        # draw ith convex hull object
        # hullArea = cv2.contourArea(hull[i])
        percent_= get_percent_of_area(hull[i],_img)
        rect_areas=[]
        
        if(percent_ > 0 and percent_< 100):
        # if(hullArea > 3000 and hullArea < 400000):

            if (img_draw):
                rect = cv2.minAreaRect(hull[i])
                rect_areas.append(rect)
                box = cv2.boxPoints(rect)
                box = np.int0(box)
                cv2.drawContours(drawing,[box],0,(0,255,255),2)
                cv2.drawContours(drawing, hull, i, color, 1, 8)

            # epsilon = 0.1*cv2.arcLength(hull[i],True)
            # approx = cv2.approxPolyDP(hull[i],epsilon,True)

            # if(img_draw):    
            #     print ('approx antes')
            #     print (approx)
            #     cv2.drawContours(drawing, [approx], 0, (255,255,255), 3)

            # desired_areas.append(cv2.boundingRect(approx))
            # desired_areas.append(approx)
            desired_areas.append(hull[i])
        # for rect2_ in rect_areas:
        # for rect_ in rect_areas:
        
        # dist1 = cv2.pointPolygonTest(contours[0], (50, 70), True)
        # print(rect_ and rect2_)
    areas_,imgs_verified = verify_same(desired_areas,img_draw)

    if(img_draw):
        # print ('approx antes')
        # print (approx)
        for img_ver in imgs_verified:
            cv2.drawContours(drawing, [img_ver], 0, (255,25,155), 3)
    

    if (img_draw):
        cv2.imshow("img", drawing)
        cv2.waitKey(0)
    return areas_

def get_mult_factor(real_dim,small_dim):

    mult_w = int(round( real_dim[0]/small_dim[0] ))
    mult_h = int(round( real_dim[1]/small_dim[1] ))
    
    return mult_w,mult_h

def get_full_image_from_slide(slide_img):
    # get image in the smallest size
    dim = slide_img.level_dimensions[(slide_img.level_count-1)]
    img = slide_img.read_region((0,0),(slide_img.level_count-1),dim)
    open_cv_image = convert_image(img)
    return open_cv_image


def get_image_from_slide(slide_img,chosed_level_img,bounding_area):
    x,y,w,h = bounding_area
    dim_img_chosed_level = slide_img.level_dimensions[chosed_level_img]
    mult_w, mult_h = get_mult_factor(dim_img_chosed_level,dim_rescaled_img)

    w=int(round(mult_w*w))
    h=int(round(mult_h*h))

    dim_highest_img = slide_img.level_dimensions[0]
    mult_x, mult_y = get_mult_factor(dim_highest_img,dim_rescaled_img)

    x=int(round(x*mult_x))
    y=int(round(y*mult_y))

    img = slide_img.read_region((x,y),chosed_level_img,(w,h)) # x,y,level,w,h

    open_cv_image = convert_image(img)
    return open_cv_image

def get_cordenates_real_img(slide_img,chosed_level_img,bounding_area):
    x,y,w,h = bounding_area
    dim_img_chosed_level = slide_img.level_dimensions[chosed_level_img]
    mult_w, mult_h = get_mult_factor(dim_img_chosed_level,dim_rescaled_img)

    w=int(round(mult_w*w))
    h=int(round(mult_h*h))

    #dim_highest_img = slide_img.level_dimensions[0]
    dim_highest_img = slide_img.level_dimensions[chosed_level_img]#teste
    mult_x, mult_y = get_mult_factor(dim_highest_img,dim_rescaled_img)

    x=int(round(x*mult_x))
    y=int(round(y*mult_y))

    return x,y,w,h

def slice_image(img,slices,level,desired,debug_mode=False):
    x_start,y_start,w,h = get_cordenates_real_img(img,0,desired)
    
    x_max = x_start + w #img.level_dimensions[0][0]
    y_max = y_start + h #img.level_dimensions[0][1]
    x_increment = ((x_max-x_start) / slices)
    y_increment = ((y_max-y_start) / slices)
    
    if(debug_mode):
        print([x_start,y_start,x_max,y_max,x_increment,y_increment])
        print(img.level_dimensions[0])

    _,_,w,h = get_cordenates_real_img(img,level,desired)
    x_show = (w / slices)
    y_show = (h / slices)

    if(debug_mode):
        print("x_max : "+ str(x_max))
        print("y_max : "+ str(y_max))
        print("x_increment : "+ str(x_increment))
        print("y_increment : "+ str(y_increment))
    
    x=x_start
    y=y_start
    count_img = 0
    while True:
        if(debug_mode):
            print("exibindo ("+str(x)+","+str(y)+")"+","+str(level)+",("+str(x_show)+","+str(y_show)+")")
            count_img+=1
        img2write = img.read_region((x,y),level,(x_show,y_show))
        writeImage(img2write,"test/"+str(x)+"_"+str(y)+".png")

        if( ( (x+x_increment+1) >= x_max) and ( (y+y_increment+1) >= y_max) ):
            break
        elif(x+x_increment+1 >= x_max):
            x=x_start
            y+=y_increment
        else:
            x+=x_increment 
    if(debug_mode):
        print("dividido em "+str(count_img)+" partes")

def verify_img2write(img,name):
    open_cv_image = convert_image(img)
    filtered_img = filter_img(open_cv_image)
    w,h = filtered_img.shape
    total_area = w*h
    n_white_pix = np.sum(filtered_img == 255)
    percent_num_white = int(round((n_white_pix*100.0/total_area)))
    if(percent_num_white <= 55):
        name = "test/"+name
    else:
        name = "trash/"+name
    cv2.imwrite(name, open_cv_image )

def slice_image_by_dim(img,dim_target,level,desired,type_classifier="", debug_mode=False):
    
    x_start,y_start,w,h = get_cordenates_real_img(img,0,desired)
    
    x_max = x_start + w #img.level_dimensions[0][0]
    y_max = y_start + h #img.level_dimensions[0][1]
    x_increment = dim_target[0]
    y_increment = dim_target[1]
    
    # if(debug_mode):
    #     print([x_start,y_start,x_max,y_max,x_increment,y_increment])
    #     print(img.level_dimensions[0])

    # Transform (dim_target) to same proportion for the level image
    dim_img_chosed_level = img.level_dimensions[level]
    dim_img_heighest_level = img.level_dimensions[0]

    mult_x =  (dim_img_chosed_level[0]*1.0 / dim_img_heighest_level[0])
    mult_y =  (dim_img_chosed_level[1]*1.0 / dim_img_heighest_level[1])
    x_show = int(round(dim_target[0]*mult_x))
    y_show = int(round(dim_target[1]*mult_y))

    # if(debug_mode):
    #     print("x_max : "+ str(x_max))
    #     print("y_max : "+ str(y_max))
    #     print("x_increment : "+ str(x_increment))
    #     print("y_increment : "+ str(y_increment))
    
    x=x_start
    y=y_start
    count_img = 0
    while True:
        if(debug_mode):
            status = "imagem %s exibindo ( %s, %s),%s,( %s , %s )"  % (str(count_img), str(x),str(y),str(level),str(x_show),str(y_show))
            # status = "imagem " + str(count_img)+ " exibindo ("+str(x)+","+str(y)+")"+","+str(level)+",("+str(x_show)+","+str(y_show)+")"
            # status = r"%10d" % (count_img)
            status = status + chr(8)*(len(status)+1)
            print status,

        count_img+=1
        img2write = img.read_region((x,y),level,(x_show,y_show))

        if(type_classifier != ""):
            image_name = type_classifier +"_"+ str(count_img) + ".png"
        else:
            image_name = count_img+".png"

        verify_img2write(img2write,image_name)
        # writeImage(img2write,"test/"+str(x)+"_"+str(y)+".png")

        if( ( (x+x_increment+1) >= x_max) and ( (y+y_increment+1) >= y_max) ):
            break
        elif(x+x_increment+1 >= x_max):
            x=x_start
            y+=y_increment
        else:
            x+=x_increment 
    if(debug_mode):
        print("\n")
        print("dividido em "+str(count_img)+" partes")
        print("\n")
    return count_img


start_time = time.time()
logging.basicConfig(level=logging.DEBUG, filename='logError.log')

total_images_saved=0
with open('dataset_img/target.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    count_slide_images = 1 

    for row in readCSV:
        try:
            print("slide image: "+str(count_slide_images)+"/130")
            slide_img = openslide.OpenSlide("dataset_img/"+row[0])

            open_cv_image = get_full_image_from_slide(slide_img)

            #rescale the image for a percent declared
            img_rescaled, dim_rescaled_img = rescale_img(open_cv_image,percent=30)

            # cv2.imshow("teste",img_rescaled)
            # cv2.waitKey(0)

            # filters to clean up noises in the image
            filtered_img = filter_img(img_rescaled)

            #return a array of boundingRect x,y,w,h
            desired_areas = get_desired_areas(filtered_img,False) 

            print("found "+str(len(desired_areas))+" desired areas")

            count_areas = 1
            for desired_area in desired_areas:
                open_cv_image = get_image_from_slide(slide_img,(slide_img.level_count-1),desired_area)

                # slice_image(slide_img,(slide_img.level_count-1),0,desired_areas[2],True)
                if(row[1] == 0 or row[1] == "0"):
                    typeOfClassifier = "is_not_metastases"
                else:
                    typeOfClassifier = "is_metastases" 

                link2save = typeOfClassifier+"/"+str(count_slide_images)+"_"+str(count_areas)+"_"
                
                imgs_saved = slice_image_by_dim(slide_img,(300,300),0,desired_area,link2save,True)
                count_areas+=1

                total_images_saved += imgs_saved
                print("salvas: "+ str(total_images_saved)+" imagens")

            count_slide_images+=1  
        except:
            logging.exception("Oops:")
            print("-----")
            print("Ocorreu um erro")
            print("-----")
            # pass
        # name_img="test/"+"img_" +"%05d" % (1,) + "_fragment_"+"%05d" % (i+1,) +".png"
        # # name_img = "img"+".png"
        # cv2.imwrite(name_img, open_cv_image)
        # i+=1
        # break

print("--- %s seconds ---" % (time.time() - start_time))

