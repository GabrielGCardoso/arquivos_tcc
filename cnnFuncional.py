import tensorflow as tf
# from keras.datasets import mnist
# from keras.utils import to_categorical
# from keras.models import Sequential
# from keras.layers import Dense, Conv2D, Flatten
import matplotlib.pyplot as plt
#download mnist data and split into train and test setsfrom keras.datasets import mnist
# import numpy as np
(X_train, y_train), (X_test, y_test) = tf.keras.datasets.mnist.load_data()

#plot the first image in the dataset
#reshape data to fit model
X_train = X_train.reshape(60000,28,28,1)
X_test = X_test.reshape(10000,28,28,1)

y_train = tf.keras.utils.to_categorical(y_train)
y_test = tf.keras.utils.to_categorical(y_test)


#create model
model = tf.keras.Sequential()
#add model layers
model.add(tf.keras.layers.Conv2D(64, kernel_size=3, activation="relu", input_shape=(28,28,1)))
model.add(tf.keras.layers.Conv2D(32, kernel_size=3, activation="relu"))
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(10, activation="softmax"))

#compile model using accuracy to measure model performance
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

#train the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=3)

model.predict(X_test[:4])

for img in X_test[:4]:
    plt.imshow(img)
# plot the first image in the dataset


print(y_test[:4])
