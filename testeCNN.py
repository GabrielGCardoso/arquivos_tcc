import cv2
import tensorflow as tf 
# from tensorflow.keras.models import Sequential
import pickle
# from keras.models import tf.keras.models.model_from_json
# from keras.models import tf.keras.models.load_model
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import time

# def prepare(file):
#     IMG_SIZE = 130
#     img_array = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
#     new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
#     return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)

# Opening the files about data
start_time = time.time()

train_path = "test/train"
test_path = "test/test"
valid_path = "test/valid"

train_batches = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255).flow_from_directory(train_path,target_size=(300,300),classes=['is_metastases','is_not_metastases'],shuffle=False,class_mode="sparse")#batch_size=10,
test_batches = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255).flow_from_directory(test_path,target_size=(300,300),classes=['is_metastases','is_not_metastases'],shuffle=False,class_mode="sparse")#batch_size=10,
valid_batches = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255).flow_from_directory(valid_path,target_size=(300,300),classes=['is_metastases','is_not_metastases'],shuffle=False,class_mode="sparse")#batch_size=10,
# X = pickle.load(open("X.pickle", "rb"))
# y = pickle.load(open("y.pickle", "rb"))

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

# # normalizing data (a pixel goes from 0 to 255)
# X_train = X_train/255.0

img = cv2.imread("/media/gabriel/TOSHIBA EXT/TCC/test/test/train/is_metastases/1_1__75.png",cv2.IMREAD_COLOR)

# Building the model
model = tf.keras.models.Sequential()
# 3 convolutional layers
model.add(tf.keras.layers.Conv2D(32, (3, 3), input_shape = img.shape))
model.add(tf.keras.layers.Activation("relu"))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))
# model.add(tf.keras.layers.Flatten())

model.add(tf.keras.layers.Conv2D(64, (3, 3)))
model.add(tf.keras.layers.Activation("relu"))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))

model.add(tf.keras.layers.Conv2D(64, (3, 3)))
model.add(tf.keras.layers.Activation("relu"))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))
model.add(tf.keras.layers.Dropout(0.25))

# 2 hidden layers
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(32))
model.add(tf.keras.layers.Activation("relu"))

model.add(tf.keras.layers.Dense(16))
model.add(tf.keras.layers.Activation("relu"))

# The output layer with 13 neurons, for 13 classes
# model.add(tf.keras.layers.Dense(2))
model.add(tf.keras.layers.Activation("softmax"))
model.add(tf.keras.layers.Dense(2, activation='softmax'))

# Compiling the model using some basic parameters
model.compile(loss="sparse_categorical_crossentropy",
				optimizer="adam",
				metrics=["accuracy"])

# model.compile(Adam(lr =.0001 ),loss="categorical_crossentropy",metrics=["accuracy"])
# Training the model, with 40 iterations
# validation_split corresponds to the percentage of images used for the validation phase compared to all the images
history = model.fit(train_batches,shuffle=False, validation_data=valid_batches, epochs=20,verbose=2)

print("-------------- acuracia teste -----------------------")
print("-------------------------------------")
model.evaluate(test_batches)
print("--------------FIM-----------------------")

# Saving the model
model_json = model.to_json()
with open("model.json", "w") as json_file :
	json_file.write(model_json)

model.save_weights("model.h5")
print("Saved model to disk")

model.save('CNN.model')

# Printing a graph showing the accuracy changes during the training phase
print(history.history.keys())
plt.figure(1)
print(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')

print("--- %s seconds ---" % (time.time() - start_time))

# CATEGORIES = ["dv",'cat']
# image = prepare("/home/gabriel/Documentos/imgs2/00000.jpg") #your image path
# prediction = model.predict(image)
# prediction = list(prediction[0])
# print(prediction)
# print(CATEGORIES[prediction.index(max(prediction))])