from PIL import Image
import openslide
import cv2
import numpy as np

def convert_image(img):
    pil_image = img.convert('RGB')
    open_cv_image = np.array(pil_image)
    # Convert RGB to BGR
    open_cv_image = open_cv_image[:, :, ::-1].copy()
    return open_cv_image

def writeImage(img,name):
    open_cv_image = convert_image(img)
    cv2.imwrite(name, open_cv_image )

def imgShow(img,name):
    open_cv_image = convert_image(img)
    cv2.imshow(name,open_cv_image)

def slice_image(img,slices,level):
    x_max = img.level_dimensions[0][0]
    y_max = img.level_dimensions[0][1]
    x_increment = x_max / slices
    y_increment = y_max / slices
    
    x_show = img.level_dimensions[level][0]
    x_show = x_show / slices
    y_show = img.level_dimensions[level][1]
    y_show = y_show / slices

    print("x_max : "+ str(x_max))
    print("y_max : "+ str(y_max))
    print("x_increment : "+ str(x_increment))
    print("y_increment : "+ str(y_increment))
    
    x=0
    y=0
    while True:
        print("exibindo ("+str(x)+","+str(y)+")"+","+str(level)+",("+str(x_show)+","+str(y_show)+")")
        img2write = img.read_region((x,y),level,(x_show,y_show))
        writeImage(img2write,"test/"+str(x)+"_"+str(y)+".png")
        
        if( ( (x+x_increment+1) >= x_max) and ( (y+y_increment+1) >= y_max) ):
            break
        elif(x+x_increment+1 >= x_max):
            x=0
            y+=y_increment
        else:
            x+=x_increment        
        
        


img = openslide.OpenSlide('HobI17-213453116848.svs')

slice_image(img,2,2)

# pegando imagem no tamanho total
# dim = img.level_dimensions[2]
# img = img.read_region((0,0),2,dim)
# x=2738-1000
# y=2274-1000

# img1 = img.read_region((10000,10000),2,(800,800))
# imgShow(img1,'img1')
# img2 = img.read_region((0,0),2,(800,800))
# imgShow(img2,'img2')

# open_cv_image = convert_image(img)

# cv2.imwrite( "teste.png", open_cv_image )
# cv2.imshow('image',open_cv_image)
# cv2.waitKey(0)