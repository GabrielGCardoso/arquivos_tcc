from PIL import Image
import openslide
import cv2
import numpy as np
import imutils
# from pprint import pprint

def convert_image(img):
    pil_image = img.convert('RGB')
    open_cv_image = np.array(pil_image)
    # Convert RGB to BGR
    open_cv_image = open_cv_image[:, :, ::-1].copy()
    return open_cv_image

def writeImage(img,name):
    open_cv_image = convert_image(img)
    cv2.imwrite(name, open_cv_image )

def imgShow(img,name):
    open_cv_image = convert_image(img)
    cv2.imshow(name,open_cv_image)
        

# def inverte(imagem, name):
#     imagem = abs(imagem - 255)
#     cv2.imwrite(name, imagem)
def inverte(imagem, name):
    imagem = (255-imagem)
    cv2.imwrite(name, imagem)

def inverte2(imagem, name):
    imagem = cv2.bitwise_not(imagem)
    # for x in np.nditer(imagem, op_flags=['readwrite']):
    #     x = abs(x - 255)
    cv2.imwrite(name, imagem)

def _get_tip_position(array, contour, verbose = False):
    approx_contour = cv2.approxPolyDP(contour, 0.08 * cv2.arcLength(contour, True), True)
    convex_points  = cv2.convexHull(approx_contour, returnPoints = True)
    
    cx, cy     = 999, 999

    for point in convex_points:
        cur_cx, cur_cy = point[0][0], point[0][1]

        if verbose:
            cv2.circle(array, (cur_cx, cur_cy), 4, _COLOR_GREEN,4)
            
        if (cur_cy < cy):
            cx, cy = cur_cx, cur_cy

    (screen_x, screen_y) = pyautogui.size()

    height, width, _ = array.shape
    x = int(round(((float(cx))/(width-0)*(screen_x+1))))
    y = int(round(((float(cy))/(height-0)*(screen_y+1))))
    return (array, (x, y)) 


img = openslide.OpenSlide('HobI17-213453116848.svs')

# slice_image(img,2,2)

# pegando imagem no tamanho total
dim = img.level_dimensions[2]
img = img.read_region((0,0),2,dim)
open_cv_image = convert_image(img)

# new_width = dim[0]/3
# new_height = dim[1]/3
scale_percent = 30 # percent of original size
width = int(open_cv_image.shape[1] * scale_percent / 100)
height = int(open_cv_image.shape[0] * scale_percent / 100)
dim = (width, height)

resized_img = cv2.resize(open_cv_image, dim, interpolation = cv2.INTER_AREA)

img = cv2.cvtColor(resized_img, cv2.COLOR_BGR2GRAY)
gray = cv2.bilateralFilter(img, 11, 17, 17)
# blur = cv2.GaussianBlur(gray,(5,5),0)
ret, thresh = cv2.threshold(gray, 130, 255, cv2.THRESH_BINARY)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
hull = []
 
# calculate points for each contour
for i in range(len(contours)):
    # creating convex hull object for each contour
    hull.append(cv2.convexHull(contours[i], False))

# create an empty black image
drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)

desired = []
# draw contours and hull points
for i in range(len(contours)):
    color_contours = (0, 255, 0) # green - color for contours
    color = (255, 0, 0) # blue - color for convex hull
    # draw ith contour
    cv2.drawContours(drawing, contours, i, color_contours, 1, 8, hierarchy)
    # draw ith convex hull object
    hullArea = cv2.contourArea(hull[i])
    if(hullArea > 7000 and hullArea < 60000):
        

        rect = cv2.minAreaRect(hull[i])
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        cv2.drawContours(drawing,[box],0,(0,255,255),2)
        cv2.drawContours(drawing, hull, i, color, 1, 8)
        
        epsilon = 0.1*cv2.arcLength(hull[i],True)
        approx = cv2.approxPolyDP(hull[i],epsilon,True)

        print ('approx antes')
        print (approx)
        desired.append(cv2.boundingRect(approx))


        # rect = cv2.minAreaRect(approx)
        # box = cv2.boxPoints(rect)
        # box = np.int0(box)
        # cv2.drawContours(drawing,[box],0,(0,0,255),2)
        # print("approx")
        # print(approx[0][0])
        # print(approx[1][0])
        # print(approx[2][0])
        # print(approx[3][0])
        # print("fim")
        
        # x_temp1 = approx[0][0][0] - approx[1][0][0] #x1-x2
        # x_temp2 = approx[3][0][0] - approx[2][0][0] #x4-x3
        # y_temp1 = approx[0][0][1] - approx[3][0][1] #y1-y4
        # y_temp2 = approx[1][0][1] - approx[2][0][1] #y2-y3

        # x_temp = x_temp1 if x_temp1 < x_temp2 else x_temp2
        # y_temp = y_temp1 if y_temp1 < y_temp2 else y_temp2

        # approx[0][0][0]= approx[2][0][0]+x_temp#x1
        # approx[0][0][1]= approx[2][0][1]+y_temp#y1
        # approx[1][0][0]= x_temp#x2
        # approx[1][0][1]= approx[2][0][1]+y_temp#y2
        # approx[3][0][0]= approx[2][0][0]+x_temp#x4
        # approx[3][0][1]= approx[2][0][1]#y4

        # print ('approx depois')
        # print (approx)

        # desired.append(cv2.boundingRect(approx))
        # print(approx[1][0])
        # print(approx[1][1])
        cv2.drawContours(drawing, [approx], 0, (255,255,255), 3)
        # print "simplified contour has",len(approx),"points"

print (desired)#x,y,w,h = cv2.boundingRect(cnt)
print(dim)

img = openslide.OpenSlide('HobI17-213453116848.svs')

# slice_image(img,2,2)

# pegando imagem no tamanho total
x,y,w,h = desired[2]
dim2 = img.level_dimensions[0]
mult_w = int(round( dim2[0]/dim[0] ))
mult_h = int(round( dim2[1]/dim[1] ))

w=int(round(mult_w*w))
h=int(round(mult_h*h))

dim2 = img.level_dimensions[0]
mult_x = int(round( dim2[0]/dim[0] ))
mult_y = int(round( dim2[1]/dim[1] ))

x=int(round(x*mult_x))
y=int(round(y*mult_y))

print('desired[0]')
print(desired[0])

print('(x,y),2,(w,h)')
print([(x,y),2,(w,h)])

img = img.read_region((x,y),0,(w,h)) # x,y,level,w,h
open_cv_image = convert_image(img)

cv2.imwrite("cortada.png", open_cv_image )

# cv2.imshow("img2", open_cv_image)
# cv2.waitKey(0)

# retval, threshold = cv2.threshold(blur, 12, 255, cv2.THRESH_BINARY)
# edged = cv2.Canny(blur, 30, 200)
# cnts = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# cnts = imutils.grab_contours(cnts)
# cnts = sorted(cnts, key = cv2.contourArea, reverse = True)
# for c in cnts:
#     cv2.drawContours(resized_img, [c], -1, (0, 255, 0), 3)


# gray = cv2.bilateralFilter(img, 11, 17, 17)

# img = cv2.Canny(dst, 30, 200)

# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
# grad = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)

# _, bw = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

# kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
# connected = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)

# contours, hierarchy = cv2.findContours(connected.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]

# # y-coordinate of midline of rectangle
# def ymid(y, h): return y+int(h/2)

# # identify lines (l=0, 1, ...) based on ymid() and estimate line width
# ym2l, l, l2w, rects = {}, 0, {}, []
# for cont in contours:
#     x, y, w, h = cv2.boundingRect(cont)
#     rects.append([x, y, w, h])
#     ym = ymid(y, h)
#     if ym not in ym2l:
#         for i in range(-2, 3):   # range of ymid() values allowed for same line 
#             if ym+i not in ym2l:
#                 ym2l[ym+i] = l
#         l2w[l] = w
#         l += 1
#     else:
#         l2w[ym2l[ym]] += w

# # combine rectangles for "good" lines (those close to maximum width)
# maxw, l2r = max(l2w.values()), {}
# for x, y, w, h in rects:
#     l = ym2l[ymid(y, h)]
#     if l2w[l] > .9*maxw:
#         if l not in l2r:
#             l2r[l] = [x, y, x+w, y+h]
#         else:
#             x1, y1, X1, Y1 = l2r[l]
#             l2r[l] = [min(x, x1), min(y, y1), max(x+w, X1), max(y+h, Y1)]

# for x, y, X, Y in l2r.values():
#     cv2.rectangle(img, (x, y), (X-1, Y-1), (255, 255, 255), 2)

cv2.imshow("img", drawing)
cv2.waitKey(0)

# gray = cv2.bilateralFilter(resized_img, 11, 17, 17)
# dst = cv2.GaussianBlur(gray,(3,3),cv2.BORDER_DEFAULT)
# edged = cv2.Canny(dst, 30, 200)

# cv2.imshow('image',edged)
# cv2.waitKey(0)
# x=2738-1000
# y=2274-1000

# img1 = img.read_region((10000,10000),2,(800,800))
# imgShow(img1,'img1')
# img2 = img.read_region((0,0),2,(800,800))
# imgShow(img2,'img2')

# open_cv_image = convert_image(img)

# cv2.imwrite( "teste.png", open_cv_image )
# cv2.imshow('image',open_cv_image)
# cv2.waitKey(0)