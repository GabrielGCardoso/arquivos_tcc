import numpy as np
import os
from matplotlib import pyplot as plt
import cv2
import random
import pickle
import time
import logging

file_list = []
class_list = []

DATADIR = "/media/gabriel/TOSHIBA EXT/TCC/test/test"

# All the categories you want your neural network to detect
CATEGORIES = ["is_not_metastases","is_metastases"]

# The size of the images that your neural network will use
IMG_SIZE = 300

start_time = time.time()
logging.basicConfig(level=logging.DEBUG, filename='logError.log')

# Checking or all images in the data folder
# for category in CATEGORIES :
# 	path = os.path.join(DATADIR, category)
# 	for img in os.listdir(path):
# 		img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
X = []
pickle_out = open("X.medicalimg", "wb")
pickle.dump(X, pickle_out)
pickle_out.close()

y=[]
pickle_out = open("y.medicalimg", "wb")
pickle.dump(y, pickle_out)
pickle_out.close()

training_data = []

def create_training_data():
	for category in CATEGORIES :
		path = os.path.join(DATADIR, category)
		class_num = CATEGORIES.index(category)
		
		total = len(os.listdir(path))
		
		i=0
		for img in os.listdir(path):

			pickle_in = open("X.medicalimg", "rb")
			X = pickle.load(pickle_in)

			pickle_in = open("y.medicalimg", "rb")
			y = pickle.load(pickle_in)

			training_data=[]
			if(i<1000):
				i+=1
				try :
					status = "imagem %s de total %s"  % (str(i), str(total))
					# status = "imagem " + str(count_img)+ " exibindo ("+str(x)+","+str(y)+")"+","+str(level)+",("+str(x_show)+","+str(y_show)+")"
					# status = r"%10d" % (count_img)
					status = status + chr(8)*(len(status)+1)
					print status,
					img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
					# new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
					training_data.append([img_array, class_num])
				except Exception as e:
					logging.exception("Oops:")
					print(e)
					pass
					
				random.shuffle(training_data)

				X_temp = [] #features
				y_temp = [] #labels
				# print(X)
				# print(y)
				for features, label in training_data:
					X_temp.append(features)
					y_temp.append(label)

				# X_temp = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

				X.extend(X_temp)
				y.extend(y_temp)
				# if(i==2):
				# print(X)
				# Creating the files containing all the information about your model
				
			else:
				print("\n")
				print("rodou "+str(i)+"\n")
				pickle_out = open("X.medicalimg", "wb")
				pickle.dump(X, pickle_out)
				pickle_out.close()

				pickle_out = open("y.medicalimg", "wb")
				pickle.dump(y, pickle_out)
				pickle_out.close()
				i=0	


create_training_data()

# pickle_in = open("X.medicalimg", "rb")
# X = pickle.load(pickle_in)

# X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

# pickle_out = open("X.medicalimg", "wb")
# pickle.dump(X, pickle_out)
# pickle_out.close()		

# print(X)	



print("--- %s seconds ---" % (time.time() - start_time))